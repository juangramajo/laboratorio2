#Strings  "str"
print("Hola mundo")
print(type("Hola mundo"))

#Concatenacion de caracteres con el operador "+"
print("bye "+"World")

#Numeros
#Enteros "Int"
print(30)
print(type(30))

# Decimales "Float"
print(1.5)
print(type(1.5))

#Booleano "Bool"
True
False

print(type(True))
print(type(False))

#Listas "list" (Se Pueden cambiar) (Van entre corchetes "[]")
[10,20,55]
["hello","bye",]
[10, "Hello", True, 10.5]
[]#(Lista vacia)
print(type([10, "Hello", True, 10.5]))


#Tuplas "tuple" (No se pueden cambiar son "Inmutable") (Van entre parentesis "()")
(10,20,55)
print(type((10,20,55)))

#Diccionarios "dict"
{
 #clave   "valor"
   #🡣      🡣
"Nombre": "Juan",
"Apellido": "Gramajo",
"Apodo":"Juanchi"
}
print(type({
"Nombre": "Juan",
"Apellido": "Gramajo",
"Apodo":"Juanchi"
}))

#None
None
print(type(None))


