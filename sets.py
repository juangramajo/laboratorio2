colores = {"rojo", "verde", "azul"}

print(colores)
print("rojo" in colores)
colores.add("violeta")
print(colores)

colores.remove("rojo")
print(colores)

colores.clear()
print(colores)