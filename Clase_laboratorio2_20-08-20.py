
# Pedir al usuario un tipo de temperatura (celsius[C] o Fahrenheit[F])
# Pedir al usuario la temperatura
# Convertir a la otra temperatura

# C = (5*(F-32))/9
# F = (9*C + 32*5)/5

tipo =input("Ingrese un tipo de temperatura 'C' para celsius o 'F' para Farenheit\n").upper()


temp = float (input("Ingrese la temperatura\n"))

if tipo == 'C':
   temp =  (temp *9/5)+32
   print('La temperatura en C° es :',temp)
elif tipo == 'F':
    temp = (temp -32) *5/9
    print('La temperatura en F° es :',temp)
else:
    print('Valor no encontrado\n')


# Pedir un mes al usuario (Enero-diciembre)
# Mostrar a que estacion pertenece (verano, otoño, invierno, primavera)
# Verano: Enero, Febrero, Marzo
# Otoño: Abril, Mayo, Junio
# Invierno: Julio, Agosto, Septiempre
# Primavera: Octubre, Noviembre, Diciembre

mes = input("Ingrese el mes\n").upper()

if mes is 'ENERO' :
        print('Es verano')
elif mes is 'FEBRERO':
        print('Es verano')
elif mes is 'MARZO':
    print('Es verano')
elif mes is 'ABRIL':
    print('Es otoño')
elif mes is 'MAYO':
    print('Es otoño')
elif mes is 'JUNIO':
    print('Es otoño')
elif mes is 'JULIO':
    print('Es invierno')
elif mes is 'AGOSTO':
    print('Es invierno')
elif mes is 'SEPTIEMBRE':
    print('Es invierno')
elif mes is 'OCTUBRE':
    print('Es primavera')
elif mes is 'NOVIEMBRE':
    print('Es primavera')
elif mes is 'DICIEMBRE':
    print('Es primavera')
else :
    print("Valor no encontrado")

# OTRA FORMA MAS FACIL

mes = input('Ingrese mes: ').lower()

if mes in ('enero', 'febrero', 'marzo'):
    print('Verano')
elif mes in ('abril', 'mayo', 'junio'):
    print('otoño')
elif mes in ('julio', 'agosto', 'septiembre'):
    print('invierno')
elif mes in ('octubre', 'noviembre', 'diciembre'):
    print('primavera')  

# Pedir un numero entero al usuario
# Mostrar tabla de multiplicar


num =int( input('Ingrese un numero\n'))

for i in range(1, 11):
    num1 = num*i
    print(num1)

# Pedir numero entero al usuario
# Mostrar el pantalla en el siguiente formato
# '''
# *
# **
# ***
# ****
# *****
# '''


num = int (input('Ingrese un numero\n'))
char = '*'
for i in range(1,num+1):
    print(i*"*")
