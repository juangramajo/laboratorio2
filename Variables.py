#Asignacion de Variables
#(No pueden comenzar con números, es case sensitive (distingue mayúsculas y minúsculas)
#los nombres sólo pueden contener letras, números y barras bajas (solo barras bajas, 
#los guiones te crearán un error de sintaxis).)
name = "Juan"
edad = 27
numeros_de_la_suerte = [7,5,3,573]

print(name)
print(edad)
print(numeros_de_la_suerte)

#------- Otra forma de escribir las variables es, un una sola linea ----------------

x,Libro = 100,"El Psicoanalista"

print(x)
print(Libro)

#------- Otra forma de imprimir las variables es, un una sola linea ----------------

print(x,Libro)

#Convenciones a la hora de escribir variables

libro_nombre = "El Psicoanalista" #Snake case - Mas utilizada en Python
libroNombre = "El Psicoanalista" #Camel case - Tambien se usa bastante
LibroNombre = "El Psicoanalista" #Pascal case

#Constantes en Python (Valor que nunca cambia)
#Se escriben siempre en mayúsculas, el interprete no hace diferencia, por eso también
#esto es una convención
PI  = 3.1416
MY_NOMBRE = "Juan"

# En Python se pueden reasignar el valor de las variables
#Por eso decimos que es un lenguaje de tipado dinámico

nombre = "Juan" #Primer valor de la variable "nombre"
nombre = "Esteban" #Nuevo y último valor de la variable "nombre"

print(nombre)




