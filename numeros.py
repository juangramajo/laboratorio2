# #OPERACIONES CON NUMEROS

# #SUMA
print(1 + 2)

# #RESTA
print(2 - 1)

# #MULTIPLICACION
print(3 * 2)

# #DIVISIÓN
print(10 / 2)

# #MÓDULO
print(10 % 2)
print(5 // 3)
# #PPOTENCIACIÓNprint(5 ** 3)

#COMO OPERAR CON NÚMEROS RECIBIENDO LOS DATOS MEDIANTE UN input()

edad = input("Inserte su edad: ")
nueva_edad = int(edad) + 5 #convierte en entero el string recibido por el input()
print(nueva_edad)
