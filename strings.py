mistr = "Hola Mundo"

#print(dir(mistr)) #Muestra todo lo que podemos hacer con este tipo de dato

print(mistr.upper()) #Convierte todo el string en mayúscula
print(mistr.lower()) #Convierte todo el string en minúscula
print(mistr.swapcase()) ##Convierte lo que esta en mayuscula en minuscula y viceversa
print(mistr.capitalize()) #Convierte el string con letra capital(la primera letra en mayuscula)
print(mistr.replace('Hola', 'Adios'))# Reemplaza un valor del strng por otro que le indiquemos
print(mistr.replace('Hola', 'Adios').upper())#También pueden usarse varios métodos a la vez "metodos encadenados"
print(mistr.count("l"))#Cuenta la cantidad de veces que se usa un caracter en el atring
print(mistr.startswith('Hello'))# Responde a la pregunta "mistr, Empieza con la palabra 'Hello'(O lo que coloquemos en los parentecis)?"
print(mistr.endswith('World')) # Responde a la pregunta "mistr, Termina con la palabra 'World'(O lo que coloquemos en los parentecis)?"
print(mistr.split())#Divide el string
print(mistr.find('a')) #Busca la posicion del indice de la letra "a" (o de la que le indiquemos)
print(len(mistr))#Devuelve la longitud del string (comienza desde el índice cero (0))
print(mistr.index('o')) #Devuelve cual es el indice de la letra "o" (o de la que le indiquemos) (comienza dese el cero (0))
print(mistr.isnumeric())# Responde si "mistr" es numerico
print(mistr.isalnum())# Responde si "mistr" es alfanumerico
print(mistr[3])#Imprime el valor del indice del string que le indiquemos (en este caso es la letra "a")

#Formas de concatenar strings

print("Quisiera decir " + mistr)
print(f"Quisiera decir {mistr}")
print("Quisiera decir {0}".format(mistr))