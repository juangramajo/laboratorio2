#COMO CREAR LISTAS

lista_numeros = list((1,2,3,4))# Siempre deben ir dendro de una tupla "entre parentesis dentro de los parentecis de list"
print(lista_numeros)

colores = ["verde","azul", "amarillo"]

rango = list(range(1,100)) # De esta manera imprime los numero dentro de un rango que le indiquemos(en este caso del 1 al 100)
print(rango)

print(type(rango))# Indica que tipo de dato es
print(dir(rango))# Indica todo lo que podemos hacer con este tipo de dato
print(len(rango)) # Indica la longitud de la lista

print("verde" in colores) #cComprueva si "verde" esta en la lista "colores"

#En las listas podemos reemplazar los valores de la misma

print(colores)
colores [1] = "rojo"
print(colores)

#Se pueden también agragar elementos

colores.append("violeta")
print(colores)

# Asi se agregan mas de un elemento a las listas

colores.extend(("naranja","rosado"))
print(colores)

#Asi insertamos un elemento en las listas indicandole el indice

colores.insert(1, "negro")
print(colores)

#Asi quitamos el último elemento de un lista

colores.pop()
print(colores)
#Asi quitamos el elemento de un lista indicandole el indice

colores.pop(1)
print(colores)

#Asi quitamos el elemento de un lista que le indiquemos

colores.remove("rojo")
print(colores)

#Asi quitamos todos los elemento de un lista

#colores.clear()
print(colores)

#Asi ordenamos los elemento de un lista

colores.sort()
print(colores)

#Asi ordenamos los elemento de un lista al revés

colores.sort(reverse = True)
print(colores)

#Asi imprimimos el indice de los elemento de un lista 

print(colores.index("amarillo"))

#Asi contamos las veces que se repite un elemento de un lista 

print(colores.count("amarillo"))