

menu = '''          
             ### MENÚ ###
-1 Calcular el aerea de un triangulo
-2 Calcular el perimetro de un triangulo
-3 Saber que tipo de triangulo es
-4 Salir
   '''
   
opcion = True
while opcion == True :
   print(menu)
   op = int (input("Ingrese una opcion\n"))

   if op is 1 :
      base = float(input("Ingrese la base del triangulo\n"))
      altura = float(input("Ingrese la altura del triangulo\n"))
      area = (base * altura) / 2

      print("El area del triangulo es: " + str(area))
      print("\n")
   elif op is 2:
      lado1 = float(input("Ingrese el lado 1 del Triangulo\n"))
      lado2 = float(input("Ingrese el lado 2 del Triangulo\n"))
      lado3 = float(input("Ingrese el lado 3 del Triangulo\n"))
      perimetro = lado1 + lado2 + lado3
      print("El perimetro del triangulo es: " + str(perimetro))
      print("\n")
   elif op is 3:
      lado1 = float(input("Ingrese el lado 1 del Triangulo\n"))
      lado2 = float(input("Ingrese el lado 2 del Triangulo\n"))
      lado3 = float(input("Ingrese el lado 3 del Triangulo\n"))
      if lado1 == lado2 and lado1 == lado3:
            print("El triangulo es EUILATERO\n")
      if lado1 == lado2 and lado1 != lado3:
            print("El triangulo es ISÓSCELES\n")
      if lado1 != lado2 and lado1 != lado3:
            print("El triangulo es ESCALENO\n")

   elif op is 4:
      print("Saliste")
      opcion = False
   else :
      print("Valor no encontrado")


     