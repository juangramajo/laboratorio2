# EJEMPLO 1
x = 10

if x < 20:
    print("x es menor que 20")
else:
    print("x es mayor que 20")

#EJEMPLO 2 USANDO ELIF
color = "rojo"

if color == "azul":
    print("El color es azul")
elif color == "rojo":
    print("El color es rojo")
else:
    print("Es de otro color")

#EJEMPLO 3
nombre = "Juan"
Apellido = "Gramajo"

if nombre == "Juan":
    if Apellido == "Gramajo":
        print("Tu eres Juan Gramajo")
    else:
        print("Tu no eres Juan Gramajo")
else:
    print("Tu no eres Juan")



#EJEMPLR 4

x = int (input("Inggrese el valor de x \n"))

if x > 10 and x <= 20:
    print("x esta entre 10 y 20 ")
elif x > 0 and x <= 10:
    print("x esta entre 0 y 10")
else:
    print("x es mayor que 20 o es un número negativo")
    